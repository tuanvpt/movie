package com.example.moviehome.response;


import com.example.moviehome.models.MovieModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

// This class is for getting multiple movies
public class MovieSearchResponse {
    @SerializedName("total_results")
    private int totalCount;

    @SerializedName("results")
    private List<MovieModel> movies;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<MovieModel> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieModel> movies) {
        this.movies = movies;
    }
}
