package com.example.moviehome;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.moviehome.databinding.ActivityMainBinding;
import com.example.moviehome.models.MovieModel;
import com.example.moviehome.network.MovieApi;
import com.example.moviehome.request.Service;
import com.example.moviehome.response.MovieSearchResponse;
import com.example.moviehome.utils.Constants;
import com.example.moviehome.viewmodels.MovieListViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieListActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityMainBinding binding;


    //ViewModel
    private MovieListViewModel movieListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        // binding.btnCheck.setOnClickListener(this);

        movieListViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        // Calling the observers

        ObserveAnyChange();

        binding.btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchMovieApi("Fast", 1);
            }
        });


    }


    // 4. Calling method in Main activity

    private void searchMovieApi(String query, int pageNumber) {
        movieListViewModel.searchMovieApi(query, pageNumber);
    }

    //Testing the method

    @Override
    public void onClick(View view) {
        // GetRetrofitResponse();
        GetRetrofitResponseAccordingToID();
    }

    private void GetRetrofitResponse() {
        MovieApi movieApi = Service.getMovieAPI();

        Call<MovieSearchResponse> responseCall = movieApi.searchMovie(
                Constants.API_KEY,
                "Action",
                1
        );

        responseCall.enqueue(new Callback<MovieSearchResponse>() {
            @Override
            public void onResponse(Call<MovieSearchResponse> call, Response<MovieSearchResponse> response) {

                if (response.code() == 200) {

                    Log.v("TAG", "the response" + response.body().toString());

                    List<MovieModel> movies = new ArrayList<>(response.body().getMovies());
                    for (MovieModel movie : movies) {
                        Log.v("TAG", "THE LIST" + movie.getReleaseDate());
                    }

                } else {
                    try {
                        Log.v("TAG", "Error" + response.errorBody().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(Call<MovieSearchResponse> call, Throwable t) {

            }
        });


    }

    private void GetRetrofitResponseAccordingToID() {
        MovieApi movieApi = Service.getMovieAPI();
        Call<MovieModel> responseCall = movieApi.getMovie(343611, Constants.API_KEY);

        responseCall.enqueue(new Callback<MovieModel>() {
            @Override
            public void onResponse(Call<MovieModel> call, Response<MovieModel> response) {
                if (response.code() == 200) {
                    MovieModel movie = response.body();
                    Log.v("Tag", "The response: - " + movie.getTitle());
                } else {
                    try {
                        Log.v("Tag", "Error" + response.errorBody().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieModel> call, Throwable t) {

            }
        });
    }

    // Observing any data change
    private void ObserveAnyChange() {

        movieListViewModel.getMovies().observe(this, new Observer<List<MovieModel>>() {
            @Override
            public void onChanged(List<MovieModel> movieModels) {
                // Observing for any change


            }
        });

    }

}