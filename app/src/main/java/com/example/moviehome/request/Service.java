package com.example.moviehome.request;

import com.example.moviehome.BuildConfig;
import com.example.moviehome.network.MovieApi;
import com.example.moviehome.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Service {

    private static final long READ_TIMEOUT = 60;
    private static final long WRITE_TIMEOUT = 60;
    private static final long CONNECTION_TIMEOUT = 60;

    private static OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        httpClientBuilder.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        httpClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            httpClientBuilder.addInterceptor(logging);
            logging.level(HttpLoggingInterceptor.Level.BODY);
        }
        return httpClientBuilder.build();
    }


    private static Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(provideOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = retrofitBuilder.build();

    private static MovieApi movieAPI = retrofit.create(MovieApi.class);

    public static MovieApi getMovieAPI() {
        return movieAPI;
    }

    public static void setMovieAPI(MovieApi movieAPI) {
        Service.movieAPI = movieAPI;
    }


}
