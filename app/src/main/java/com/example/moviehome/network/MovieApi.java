package com.example.moviehome.network;

import com.example.moviehome.models.MovieModel;
import com.example.moviehome.response.MovieSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApi {

    // https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher
    // Search for movies
    @GET("3/search/movie")
    Call<MovieSearchResponse> searchMovie(
            @Query("api_key") String key,
            @Query("query") String query,
            @Query("page") int page
    );


    // Making Search with ID
    //
    //Remember that movie_id = 550 is for Jack Reacher movie
    @GET("3/movie/{movie_id}?")
    Call<MovieModel> getMovie(
            @Path("movie_id") int movieId,
            @Query("api_key") String key
    );
}
