package com.example.moviehome.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import com.example.moviehome.dialog.LoadingDialog;
import com.example.moviehome.utils.Utilities;


public abstract class BaseActivity<VB extends ViewBinding, VM extends BaseViewModel> extends AppCompatActivity {
    protected VM viewModelX;
    private LoadingDialog loadingDialog;
    protected VB binding;

    public abstract VB getLayoutBinding();

    public abstract VM getViewModel();

    protected abstract void initialize();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getLayoutBinding();
        setContentView(binding.getRoot());

        viewModelX = getViewModel();
        loadingDialog = new LoadingDialog(this);

        viewModelX.errorObs.observe(this, message -> Utilities.showToast(this, message));
        viewModelX.loadingObs.observe(this, this::changeLoadingState);

        initialize();
    }

    protected void changeLoadingState(boolean isLoading) {
        if (isLoading) {
            loadingDialog.show();
        } else {
            loadingDialog.dismiss();
        }
    }
}
