package com.example.moviehome.base;

import androidx.lifecycle.MutableLiveData;

public class BaseViewModel {

    protected MutableLiveData<String> errorObs = new MutableLiveData<>();
    protected MutableLiveData<Boolean> loadingObs = new MutableLiveData<>();
}
