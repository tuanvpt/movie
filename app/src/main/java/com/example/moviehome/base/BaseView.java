package com.example.moviehome.base;

public interface BaseView {
    void showLoading();

    void hideLoading();

    void onError(String error);
}
