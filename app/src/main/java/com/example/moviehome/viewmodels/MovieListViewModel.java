package com.example.moviehome.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.moviehome.models.MovieModel;
import com.example.moviehome.repository.MovieRepository;

import java.util.List;

public class MovieListViewModel extends ViewModel {

    // This class is used for ViewModel

    MovieRepository movieRepository;
    private MutableLiveData<List<MovieModel>> mMovies = new MutableLiveData<>();

    //Constructor
    public MovieListViewModel(MovieRepository movieRepository, MutableLiveData<List<MovieModel>> mMovies) {
        this.movieRepository = movieRepository;


    }


    public LiveData<List<MovieModel>> getMovies() {
        return mMovies;
    }

    public void searchMovieApi(String query, int pageNumber) {
        movieRepository.searchMovieApi(query, pageNumber);

    }


}
